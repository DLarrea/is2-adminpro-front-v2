import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProyectosComponent } from './proyectos/proyectos.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { RolesComponent } from './roles/roles.component';
import { ProyectosPersonalesComponent } from './proyectos/proyectos-personales/proyectos-personales.component';
import { ProyectosTodosComponent } from './proyectos/proyectos-todos/proyectos-todos.component';
import { ProyectosAgregarComponent } from './proyectos/proyectos-agregar/proyectos-agregar.component';

@NgModule({
  declarations: [
    AppComponent,
    ProyectosComponent,
    UsuariosComponent,
    RolesComponent,
    ProyectosPersonalesComponent,
    ProyectosTodosComponent,
    ProyectosAgregarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
